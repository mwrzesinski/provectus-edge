import aioboto3

from asyncio import sleep, Queue
from decimal import Decimal
from structlog import get_logger
from uuid import uuid4

from ..config import Config
from .models import Measurement


async def writer_task(config: Config, buffer: Queue[Measurement]):

    logger = get_logger(__name__, device=config.device_name, sensor=config.sensor_name)

    async with aioboto3.resource('dynamodb',
                                 region_name=config.dynamodb.region_name,
                                 aws_access_key_id=config.dynamodb.aws_access_key_id,
                                 aws_secret_access_key=config.dynamodb.aws_secret_access_key) as dynamodb:

        table = await dynamodb.Table(config.dynamodb.table_name)

        running = True
        while running:

            async with table.batch_writer() as batch:

                while not buffer.empty():
                    item = buffer.get_nowait()

                    await batch.put_item(Item={
                        'id': str(uuid4()),
                        'device': config.device_name,
                        'sensor': config.sensor_name,
                        'timestamp': item.timestamp.strftime('%Y-%m-%dT%H:%M:%S.%f%z'),
                        'values': [{
                            'label': v.label,
                            'current': Decimal(v.current),
                            'high': Decimal(v.high),
                            'critical': Decimal(v.critical),
                        } for v in item.values]
                    })

                logger.debug('Batch sent to database.')

            await sleep(config.write_interval)
