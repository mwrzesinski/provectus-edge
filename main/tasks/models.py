from dataclasses import dataclass
from datetime import datetime


@dataclass
class MeasurementValue:
    label: str
    current: float
    high: float
    critical: float

    @classmethod
    def from_dict(cls, data: dict) -> 'MeasurementValue':
        return cls(**data)


@dataclass
class Measurement:
    timestamp: datetime
    values: list[MeasurementValue]

    @classmethod
    def from_dict(cls, data: dict) -> 'Measurement':
        return cls(
            timestamp=datetime.strptime(data['timestamp'], '%Y-%m-%dT%H:%M:%S.%f'),
            values=[MeasurementValue.from_dict(d) for d in data['values']]
        )
