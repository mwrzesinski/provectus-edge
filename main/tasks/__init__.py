import aioboto3

from aiohttp.web import Application
from aiojobs.aiohttp import setup as setup_jobs, get_scheduler_from_app
from asyncio import Queue
from boto3.dynamodb.conditions import Attr
from datetime import datetime

from ..config import Config
from .models import Measurement
from .reader import reader_task
from .writer import writer_task


def setup_tasks(app: Application):
    config: Config = app['config']

    app['buffer'] = Queue(maxsize=config.buffer_size)

    setup_jobs(app)

    async def on_startup(_app: Application):
        await start_reader(_app)
        await start_writer(_app)

    async def on_shutdown(_app: Application):
        pass

    app.on_startup.append(on_startup)
    app.on_shutdown.append(on_shutdown)


async def start_reader(app: Application):
    job = app.get('reader')

    if job is not None and (job.active or job.pending):
        raise RuntimeError('Job is still running.')

    scheduler = get_scheduler_from_app(app)

    config: Config = app['config']
    buffer: Queue[Measurement] = app['buffer']

    app['reader'] = await scheduler.spawn(reader_task(config, buffer))


async def stop_reader(app: Application):
    job = app.get('reader')

    if job is None or job.closed:
        raise RuntimeError('Job is not running.')

    await job.close()
    await job.wait()


async def start_writer(app: Application):
    job = app.get('writer')

    if job is not None and (job.active or job.pending):
        raise RuntimeError('Job is still running.')

    scheduler = get_scheduler_from_app(app)

    config: Config = app['config']
    buffer: Queue[Measurement] = app['buffer']

    app['writer'] = await scheduler.spawn(writer_task(config, buffer))


async def stop_writer(app: Application):
    job = app.get('writer')

    if job is None or job.closed:
        raise RuntimeError('Job is not running.')

    await job.close()
    await job.wait()


async def query_values(app: Application, start: datetime, end: datetime, limit: int) -> list[Measurement]:
    # TODO: This should be initialized once at app startup

    config: Config = app['config']

    async with aioboto3.resource('dynamodb',
                                 region_name=config.dynamodb.region_name,
                                 aws_access_key_id=config.dynamodb.aws_access_key_id,
                                 aws_secret_access_key=config.dynamodb.aws_secret_access_key) as dynamodb:

        table = await dynamodb.Table(config.dynamodb.table_name)

        response = await table.scan(
            Limit=limit,
            FilterExpression=Attr('timestamp').between(
                start.strftime('%Y-%m-%dT%H:%M:%S.%f'),
                end.strftime('%Y-%m-%dT%H:%M:%S.%f')
            )
        )

    items = response['Items']
    return [Measurement.from_dict(d) for d in items]
