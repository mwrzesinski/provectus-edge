from asyncio import sleep, Queue, QueueFull
from dataclasses import asdict
from datetime import datetime
from psutil import sensors_temperatures
from structlog import get_logger

from ..config import Config
from .models import Measurement, MeasurementValue


async def reader_task(config: Config, buffer: Queue[Measurement]):

    logger = get_logger(__name__, device=config.device_name, sensor=config.sensor_name)

    running = True
    while running:

        temps = sensors_temperatures()
        timestamp = datetime.now()

        names = tuple(temps.keys())

        if config.sensor_name not in names:
            logger.error('Sensor not found.', sensors=names)
            running = False
            # TODO: crash application?
            continue

        # Removing duplicate labels
        values: list[MeasurementValue] = list()
        for (label, *rest) in temps[config.sensor_name]:
            if next((v for v in values if v.label == label), None) is None:
                values.append(MeasurementValue(label, *rest))

        try:
            buffer.put_nowait(Measurement(
                timestamp=timestamp,
                values=values
            ))

            logger.debug('Sensor values.', values=[asdict(v) for v in values], buffer_count=buffer.qsize())
        except QueueFull:
            logger.error('Buffer is full.', buffer_count=buffer.qsize())

        await sleep(config.read_interval)
