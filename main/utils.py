import asyncio
import json
import sys
import traceback

from aiohttp import web
from binascii import hexlify
from dataclasses import is_dataclass, asdict
from datetime import datetime
from decimal import Decimal
from enum import Enum
from typing import Any, Optional, TypeVar, Callable


T = TypeVar('T')


@web.middleware
async def error_middleware(request: web.Request, handler: Callable):

    try:
        return await handler(request)
    except Exception:
        error_type, error_exc, error_trace = sys.exc_info()

        status_code = 500
        reason = 'Internal Server Error'
        text = str(error_exc)

        if hasattr(error_exc, 'status_code'):
            status_code = error_exc.status_code
        if hasattr(error_exc, 'reason'):
            reason = error_exc.reason
        if hasattr(error_exc, 'text'):
            text = error_exc.text

        error = {
            'status': status_code,
            'reason': reason,
            'text': text,
        }

        if request.app.debug:
            lines = list()
            for line in traceback.format_exception(error_type, error_exc, error_trace):
                lines.extend(line.strip('\n').split('\n'))
            error['traceback'] = lines

        return web.Response(
            body=json.dumps({'error': error}).encode('utf-8'),
            content_type='application/json',
            status=status_code
        )


def json_response(data: Any, headers: Optional[dict] = None) -> web.Response:
    return web.Response(
        headers=headers,
        content_type='application/json',
        text=json.dumps(data, cls=JSONEncoder)
    )


async def shutdown():
    tasks = [t for t in asyncio.all_tasks() if t is not asyncio.current_task()]
    for task in tasks:
        task.cancel()

    await asyncio.gather(*tasks, return_exceptions=True)


class JSONEncoder(json.JSONEncoder):

    def default(self, obj):
        if is_dataclass(obj):
            return asdict(obj)
        if isinstance(obj, datetime):
            return obj.strftime('%Y-%m-%dT%H:%M:%S.%f')
        if isinstance(obj, Enum):
            return obj.name
        if isinstance(obj, bytes):
            return hexlify(obj).decode()
        if isinstance(obj, Decimal):
            return float(obj)
        if hasattr(obj, '__call__'):
            return None
        return super(JSONEncoder, self).default(self, obj)


class Routes(web.RouteTableDef):

    def __init__(self, initial: Optional[list] = None):
        super(Routes, self).__init__()

        if initial:
            self._items.extend(initial)
