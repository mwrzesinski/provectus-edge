# -*- coding: utf-8 -*-

__version__ = '0.0.2'

__title__ = 'provectus-edge'
__description__ = 'Provectus Edge Device Application'
__uri__ = ''

__author__ = 'Mikolaj Wrzesinski'
__email__ = 'mikolaj.wrzesinski@gmail.com'

__license__ = ''
__copyright__ = ''
