from aiohttp.web import Application
from structlog import get_logger


from .config import Config
from .logger import setup_logger
from .routes import routes
from .tasks import setup_tasks
from .utils import error_middleware


def get_app(config: Config) -> Application:
    setup_logger(config)

    logger = get_logger(__name__)

    app = Application(debug=config.debug, logger=logger, middlewares=(error_middleware,))
    app.add_routes(routes)

    app['config'] = config
    setup_tasks(app)

    return app
