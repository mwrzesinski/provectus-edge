from aiohttp.web import Request, Response, HTTPBadRequest, HTTPNoContent
from datetime import datetime

from main import __version__, __title__
from main.tasks import start_reader, stop_reader, start_writer, stop_writer, query_values
from main.utils import Routes, json_response


routes = Routes()


@routes.get('/info')
async def handler(request: Request) -> Response:
    return json_response({
        'name': __title__,
        'version': __version__,
        'ready': True,
    })


@routes.get('/values')
async def handler(request: Request) -> Response:
    if (start := request.query.get('start', None)) is None:
        raise HTTPBadRequest(text='Missing "start" parameter.')
    if (end := request.query.get('end', None)) is None:
        raise HTTPBadRequest(text='Missing "end" parameter.')
    if (limit := request.query.get('limit', None)) is None:
        raise HTTPBadRequest(text='Missing "limit" parameter.')

    try:
        _limit = int(limit)
    except ValueError:
        raise HTTPBadRequest(text='Could not convert "limit" parameter to int.')
    if _limit < 1:
        raise HTTPBadRequest(text='Limit must be higher than 0.')
    if _limit >= 100:
        raise HTTPBadRequest(text='Limit must be lower than 100.')

    values = await query_values(
        request.app,
        datetime.strptime(start, '%Y-%m-%dT%H:%M:%S'),
        datetime.strptime(end, '%Y-%m-%dT%H:%M:%S'),
        limit=_limit
    )

    return json_response(values)


@routes.post('/reader')
async def handler(request: Request) -> Response:

    data = await request.json()
    if (value := data.get('value', None)) is None:
        return HTTPBadRequest(text='Missing value.')

    if not isinstance(value, bool):
        return HTTPBadRequest(text='Value must be the "bool" type.')

    if value is True:
        await start_reader(request.app)
    else:
        await stop_reader(request.app)

    return HTTPNoContent()


@routes.post('/writer')
async def handler(request: Request) -> Response:

    data = await request.json()
    if (value := data.get('value', None)) is None:
        return HTTPBadRequest(text='Missing value.')

    if not isinstance(value, bool):
        return HTTPBadRequest(text='Value must be the "bool" type.')

    if value is True:
        await start_writer(request.app)
    else:
        await stop_writer(request.app)

    return HTTPNoContent()
