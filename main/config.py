# -*- coding: utf-8 -*-

import yaml

from dataclasses import dataclass, field
from socket import gethostname
from typing import Optional


@dataclass
class DynamoDBConfig:
    table_name: str
    region_name: str
    aws_access_key_id: str
    aws_secret_access_key: str

    @classmethod
    def from_dict(cls, data: dict) -> 'DynamoDBConfig':
        return cls(**data)


@dataclass
class Config:

    debug: bool = field(default=True)
    host: str = field(default='127.0.0.1')
    port: int = field(default=8000)
    socket: Optional[str] = field(default=None)

    device_name: str = field(default_factory=gethostname)
    sensor_name: str = field(default='coretemp')

    buffer_size: int = field(default=100)
    read_interval: float = field(default=60.0)
    write_interval: float = field(default=60.0)

    dynamodb: Optional[DynamoDBConfig] = field(default=None)

    @classmethod
    def from_dict(cls, data: dict) -> 'Config':

        _dynamodb = None
        if (ddb := data.pop('dynamodb', None)) is not None:
            _dynamodb = DynamoDBConfig(**ddb)

        return cls(**data, dynamodb=_dynamodb)

    @classmethod
    def from_file(cls, path: str) -> 'Config':
        with open(path, 'rt') as f:
            data = yaml.safe_load(f.read())
        return cls.from_dict(data)
