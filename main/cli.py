import click

from aiohttp.web import run_app
from typing import Optional

from main import __version__
from main.app import get_app
from main.config import Config
from main.logger import AccessLogger


@click.group()
@click.option('-c', '--config-path', help='Path to application config file.', default=None)
@click.version_option(version=__version__)
@click.pass_context
def cli(ctx: click.Context, config_path: Optional[str]):

    if config_path is not None:
        ctx.obj = Config.from_file(config_path)
    else:
        ctx.obj = Config.from_dict({})


@cli.command()
@click.pass_context
def run(ctx: click.Context):
    config: Config = ctx.obj

    app = get_app(config)

    run_app(
        app, host=config.host, port=config.port, path=config.socket, access_log_class=AccessLogger,
        print=lambda txt: app.logger.info(txt.split('\n')[0])
    )

    app.logger.info('Exit')
