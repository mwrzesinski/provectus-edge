import logging.config
import structlog
import sys

from aiohttp.abc import AbstractAccessLogger
from aiohttp.web import Request, Response
from pythonjsonlogger import jsonlogger

from main.config import Config


def setup_logger(config: Config):

    level = 'DEBUG' if config.debug else 'INFO'

    structlog.configure(
        processors=[
            structlog.stdlib.filter_by_level,
            structlog.stdlib.add_logger_name,
            structlog.stdlib.add_log_level,
            structlog.stdlib.PositionalArgumentsFormatter(),
            structlog.processors.StackInfoRenderer(),
            structlog.processors.format_exc_info,
            structlog.processors.UnicodeDecoder(),
            structlog.stdlib.render_to_log_kwargs,
        ],
        context_class=dict,
        logger_factory=structlog.stdlib.LoggerFactory(),
        wrapper_class=structlog.stdlib.BoundLogger,
        cache_logger_on_first_use=True,
    )

    handler = logging.StreamHandler(sys.stdout)
    handler.setFormatter(jsonlogger.JsonFormatter())
    root_logger = logging.getLogger()
    root_logger.addHandler(handler)
    root_logger.setLevel(level)


class AccessLogger(AbstractAccessLogger):

    def log(self, request: Request, response: Response, time: float):
        request.app.logger.info(
            'Request',
            remote=request.remote,
            method=request.method,
            status=response.status,
            path=request.path,
            time=time
        )
