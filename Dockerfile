FROM python:3.9.0-slim AS compile-image

RUN apt-get update \
    && apt-get install -y build-essential

RUN python -m venv /opt/venv
ENV PATH="/opt/venv/bin:$PATH"

RUN pip install -U setuptools pip

COPY requirements.txt /app/
RUN pip install -r /app/requirements.txt

COPY HISTORY.rst LICENSE README.md setup.py /app/
COPY main /app/main

RUN pip install -U --no-cache-dir /app

FROM python:3.9.0-slim AS build-image
COPY --from=compile-image /opt/venv /opt/venv

ENV PATH="/opt/venv/bin:$PATH"