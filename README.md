# Create config file #

```
echo '
debug: true
host: 0.0.0.0
port: 8000

read_interval: 3
buffer_size: 10
write_interval: 10

dynamodb:
  table_name: provectus
  region_name: us-west-2
  aws_access_key_id: "***"
  aws_secret_access_key: "***"' > ./config.yaml
```

- debug: Changes log level
- host,port - REST API exposure
- read_interval - Interval between sensor reads
- buffer_size - In-memory size of the measurments buffer
- write_interval - Interval between database writes
- dynamodb - Database data


# Run locally #

```
virtualenv -p /usr/bin/python3.9 venv
source ./venv/bin/activate
pip install -U pip setuptools
pip install -r ./requirements.txt
python -m main -c ./config.yaml run
```


# Run with docker #

```
docker run -it \
-p 8000:8000 \
-v $PWD/config.yaml:/etc/config.yaml \
mwrzesinski/provectus-edge:0.0.2 \
app -c /etc/config.yaml run
```

# API

`GET /info`

Reads application information

`GET /values?start=2020-11-04T00:00:00&end=2020-11-05T23:59:59&limit=10`

Queries sensor values directly from DynamoDB

`POST /reader`

Enables/Disables temperature sensor reading 

```
{
  "value": true/false
}
```

`POST /writer`

Enables/Disables database writing

```
{
  "value": true/false
}
```