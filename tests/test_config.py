from main.config import Config


def test_config():

    config = Config.from_dict(dict())
    assert config.debug is True
    assert config.host == '127.0.0.1'
    assert config.port == 8000
    assert config.socket is None

    assert isinstance(config.device_name, str)
    assert config.sensor_name == 'coretemp'

    assert config.buffer_size == 100
    assert config.read_interval == 60.0
    assert config.write_interval == 60.0

    config = Config.from_dict({
        'debug': False,
        'host': 'localhost',
        'port': 6666,
        'socket': '/sock',
        'device_name': 'RPi-1',
        'sensor_name': 'proc',
        'buffer_size': 10,
        'read_interval': 3.0,
        'write_interval': 11.0
    })

    assert config.debug is False
    assert config.host == 'localhost'
    assert config.port == 6666
    assert config.socket == '/sock'

    assert config.device_name == 'RPi-1'
    assert config.sensor_name == 'proc'

    assert config.buffer_size == 10
    assert config.read_interval == 3.0
    assert config.write_interval == 11.0
