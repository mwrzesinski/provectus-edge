aioboto3==8.0.5
aiohttp==3.6.2
aiojobs==0.2.2
click==7.1.2
psutil==5.7.3
python-json-logger==2.0.1
pyyaml==5.3.1
structlog==20.1.0