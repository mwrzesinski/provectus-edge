# -*- coding: utf-8 -*-

import codecs
import os
import re

from setuptools import setup, find_packages


HERE = os.path.abspath(os.path.dirname(__file__))


def read(*parts):
    """Build an absolute path from *parts* and and return the contents of the
resulting file.  Assume UTF-8 encoding."""

    with codecs.open(os.path.join(HERE, *parts), 'rb', 'utf-8') as file:
        return file.read()


###################################################################


PACKAGES = find_packages()
META_PATH = os.path.join('main', '__init__.py')
KEYWORDS = ['class']
CLASSIFIERS = [
    'Development Status :: 4 - Beta',
    'Intended Audience :: Developers',
    'Natural Language :: English',
    'Operating System :: OS Independent',
    'Programming Language :: Python',
    'Programming Language :: Python :: 3.9',
    'Topic :: Scientific/Engineering',
]
REQUIREMENTS = read('requirements.txt').split()


with open('README.md', 'r') as f:
    readme = f.read()

with open('HISTORY.rst', 'r') as f:
    history = f.read()


###################################################################


META_FILE = read(META_PATH)


def find_meta(meta):
    """Extract __*meta*__ from META_FILE."""

    meta_match = re.search(
        r"^__{meta}__ = ['\"]([^'\"]*)['\"]".format(meta=meta),
        META_FILE, re.M
    )
    if meta_match:
        return meta_match.group(1)
    raise RuntimeError('Unable to find __{meta}__ string.'.format(meta=meta))


if __name__ == '__main__':
    setup(
        name=find_meta('title'),
        version=find_meta('version'),
        description=find_meta('description'),
        long_description=read('README.md'),
        url=find_meta('uri'),
        classifiers=CLASSIFIERS,
        author=find_meta('author'),
        author_email=find_meta('email'),
        packages=PACKAGES,
        license='LICENSE',
        install_requires=REQUIREMENTS,
        include_package_data=True,
        zip_safe=False,
        entry_points={
            'console_scripts': [
                'app = main.cli:cli'
            ]
        },
    )
